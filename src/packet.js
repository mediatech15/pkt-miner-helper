const { log } = require('./utility')
const os = require('os')
const path = require('path')
const process = require('child_process')

function installPacketcrypt(password) {
    let home = os.homedir()
    let command = [
        'apt update',
        '&&',
        'apt install gcc git make curl',
        '&&',
        `cd ${home}`,
        '&&',
        "curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh",
        '&&',
        'git clone --branch develop https://github.com/cjdelisle/packetcrypt_rs',
        '&&',
        'cd packetcrypt_rs',
        '&&',
        'cargo clean',
        '&&',
        '~/.cargo/bin/cargo build --release --features jemalloc',
        '&&',
        'chmod 755 target/release/packetcrypt'
    ].join(' ')
    let proc = process.spawn(
        'sh', [
        '-c',
        `echo ${password} | sudo -S bash -c '${command}'`
    ]
    )
    proc.stdout.on('data', function (data) {
        log(data, 'INSTALL', 1, false)
    })
    proc.stderr.on('data', function (data) {
        log(data, 'INSTALL', 1, false)
    })
    proc.on('exit', function (code) {
        log(`install completed with code: ${code}`, 'INSTALL', 1, false)
    })
}

function runPacketcrypt(threads, profile, address, recover, location) {
    if (location == '') {
        let exec = path.join('./', os.homedir(), 'packetcrypt_rs', 'target', 'release', 'packetcrypt')
    } else {
        let exec = location
    }
    if (threads == null) {
        let thr = ''
    } else {
        let thr = '-t ' + threads
    }
    let command = [
        'ann',
        '-p',
        address,
        profile,
        thr
    ]
    let proc = process.spawn(exec, command)
    proc.stdout.on('data', function (data) {

    })
    proc.stderr.on('data', function (data) {

    })
    proc.on('exit', function (code) {

    })
}

module.exports = { installPacketcrypt, runPacketcrypt }