const datetime = require('node-datetime');
const colors = require('colors');

let VERBOSITY_LEVEL = 0

function log(message, caller, level, always) {
    let now = datetime.create().format('Y-m-d H:M:S.N')
    let lvl;
    if (level == 0) {
        lvl = 'DEBUG'.gray
    } else if (level == 1) {
        lvl = 'INFO '.white
    } else if (level == 2) {
        lvl = 'WARN '.yellow
    } else if (level == 3) {
        lvl = 'ERROR'.brightRed
    }
    let completeMsg = [
        '['.gray,
        now.brightWhite,
        ']'.gray,
        ' ',
        '['.gray,
        lvl,
        ']'.gray,
        ' ',
        '['.gray,
        caller.padEnd(8).cyan,
        ']'.gray,
        ' ',
        message.brightWhite
    ].join('')
    if (always) {
        console.log(completeMsg)
    } else if (level >= VERBOSITY_LEVEL) {
        console.log(completeMsg)
    }
}

function setVerbosity(level) {
    VERBOSITY_LEVEL = level
    log('verbosity level set to: ' + level, 'GLOBAL', 1, true)
}

module.exports = { log, setVerbosity, VERBOSITY_LEVEL }