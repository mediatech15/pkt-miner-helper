#!/usr/bin/env node
'use strict';

// Imports ---------------------------------
const sl = require('serverline');
const os = require('os');
const { log, setVerbosity, VERBOSITY_LEVEL } = require('./utility')
const { installPacketcrypt } = require('./packet')
const hasbin = require('hasbin');

// Globals ---------------------------------
let COMMANDS = ['help', 'verbosity', 'mining', 'install']
let PROMPT_CHAR = '$ '
let THREADS = null
let ADDRESS = null
let LOCATION = ''
let PROFILES = {
    1: 'https://stratum.zetahash.com http://pool.pkt.world http://pool.pktpool.io http://pool.pktco.in http://pool.pkteer.com',
    2: 'http://pool.pkt.world http://pool.pktpool.io http://pool.pktco.in http://pool.pkteer.com',
    3: 'http://pool.pktpool.io',
    4: 'http://pool.pkt.world',
    5: 'http://stratum.zetahash.com',
    6: 'http://pool.pktco.in',
    7: 'http://pool.pkteer.com'
}
let PROFILE = 0
let AUTO_RESTART = false
let APPDATA = process.env.APPDATA || (process.platform == 'darwin' ? process.env.HOME + '/Library/Preferences' : process.env.HOME + "/.local/share")
let PLATFORM = os.platform()

// Helper functions ------------------------
function showHelp() {
    log('help', 'HELP', 1, true)
}

// Main Handler ----------------------------
process.stdout.write('\x1Bc')
sl.init({ prompt: PROMPT_CHAR })
sl.setCompletion(COMMANDS)
sl.setPrompt(PROMPT_CHAR)

sl.on('SIGINT', function (rl) {
    rl.question('Confirm exit (y/n): ', (answer) => answer.match(/^y(es)?$/i) ? process.exit(0) : rl.output.write('\x1B[1K> '))
})

sl.on('line', function (line) {
    if (line == '') {
        return
    }
    log('command: ' + line, 'CMD IN', 1)
    if (!COMMANDS.some(s => line.startsWith(s))) {
        log('unknown command: ' + line.split(' ')[0], 'CMD IN', 3, true)
        return
    }
    if (line.startsWith('help')) {
        showHelp()
    } else if (line.startsWith('verbosity')) {
        let parts = line.split(' ')
        if (parts[1] == 'get') {
            log('verbosity level: ' + VERBOSITY_LEVEL, 'GLOBAL', 1, true)
        } else if (parts[1] == 'set') {
            try {
                let int = parseInt(parts[2])
                if (int >= 0 && int <= 3) {
                    setVerbosity(int)
                } else {
                    log('verbosity is invalid. must be 0,1,2,3', 'CMD IN', 3, true)
                }
            } catch {
                log('could not parse to int', 'CMD IN', 3, true)
            }
        } else {
            showHelp()
        }
    } else if (line.startsWith('mining')) {
        let parts = line.split(' ')
        if (parts[1] == 'start') {

        } else if (parts[1] == 'stop') {

        } else if (parts[1] == 'restart') {

        } else if (parts[1] == 'set') {
            if (parts[2] == 'profile') {

            } else if (parts[2] == 'threads') {

            } else if (parts[2] == 'address') {

            } else if (parts[2] == 'autorestart') {

            } else {
                showHelp()
            }
        } else {
            showHelp()
        }
    } else if (line.startsWith('install')) {
        if (PLATFORM == 'linux') {
            if (hasbin.sync('apt')) {
                sl.secret('Sudo password for install: ', (answer) => {
                    log('About to install packetcrypt please let it complete.', 'SYSTEM', 1, true)
                    installPacketcrypt(answer)
                })
            } else {
                log('This feature is currently only supported on linux with apt.', 'CMD IN', 2, true)
            }
        } else {
            log('This feature is currently only supported on linux with apt.', 'CMD IN', 2, true)
        }
    }
})